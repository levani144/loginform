package com.example.user.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // get reference to button
        val btnSignUp : Button = findViewById(R.id.LoginButton)
        val et_Username : EditText = findViewById(R.id.UserNameText)
        val et_password : EditText = findViewById(R.id.PasswordText)

         LoginButton.setOnClickListener {
             val user_msg_error: String = UserNameText.text.toString()

             //check if the EditText have values or not
             if(user_msg_error.trim().isEmpty()) {
                 UserNameText.error = "Required"
                 Toast.makeText(applicationContext, "User Name Required ", Toast.LENGTH_SHORT).show()
             }
             else if (et_password.text.toString().trim().isEmpty()) {
                 PasswordText.error = "Required"
                 Toast.makeText(applicationContext, "Password Required ", Toast.LENGTH_SHORT).show()
             }
             else{
                 Toast.makeText(applicationContext, "Login Successful ", Toast.LENGTH_SHORT).show()

             }
//
         }
    }

    }
